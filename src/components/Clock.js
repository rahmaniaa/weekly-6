import styles from '../css/Time.module.css';

const Clock = (props) => {
    const time = props.display.time.split(':');
    const [hours, minutes] = time;

    return (
        <>
        <h1
            className={`display-1 fw-bold text-${props.display.color} ${styles.clock}`}>
            {hours}
            <span className={styles.dot}>:</span>
            {minutes}
        </h1>
        </>
    );
};

export default Clock;