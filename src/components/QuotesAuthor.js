import styles from '../css/Quotes.module.css';

const QuotesAuthor = (props) => {
    return (
        <figcaption
        className={`${styles.author} text-${props.display.color} blockquote-footer`}>
        {props.display.quotes.author}
        </figcaption>
    );
};

export default QuotesAuthor;