import Clock from "./Clock";
import Greeting from "./Greeting";
import styles from '../css/Time.module.css';

const Time = (props) => {
    return (
        <div id={styles.time} className='align-self-center align-self-lg-start'>
            <Greeting display={props.display} />
            <Clock display={props.display} />
        </div>
    );
};

export default Time;