import styles from '../css/Time.module.css';

const Greeting = (props) => {
    return (
        <h3 className={`${styles.greeting} fw-normal text-${props.display.color}`}>
            {props.display.greeting}
        </h3>
    );
};

export default Greeting;