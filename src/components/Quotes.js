import React from "react";
import QuotesAuthor from './QuotesAuthor';
import QuotesSentence from './QuotesSentence';

const Quotes = (props) => {
    return (
        <figure>
            <QuotesSentence display={props.display} />
            <QuotesAuthor display={props.display} />
        </figure>
    );
};

export default Quotes;