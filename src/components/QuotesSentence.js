import styles from '../css/Quotes.module.css';

const QuotesSentence = (props) => {
    return (
        <blockquote
            className={`${styles.content} text-${props.display.color} blockquote`}>
            {props.display.quotes.quotes}
        </blockquote>
    );
};

export default QuotesSentence;