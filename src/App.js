import { useEffect, useState } from 'react';
import moment from 'moment';
import axios from 'axios';
import './css/App.css';
import Quotes from './components/Quotes';
import Time from './components/Time';

function App() {
  const [currentTime, setCurrentTime] = useState('Haii :D');
  const [author, setAuthor] = useState("");
  const [quotes, setQuotes] = useState("");
  const [display, setDisplay] = useState({
    background: '',
    greeting: '',
    color: '',
    quotes: { quotes, author },
    time: currentTime,
  });

  useEffect(() => {
    axios('https://api.quotable.io/random').then((result) => {
      setAuthor(result.data.author);
      setQuotes(`"${result.data.content}"`);
    });
  }, []);

  setInterval(() => {
    setCurrentTime(moment().format('HH:mm'));
  }, 1000);

  useEffect(() => {
    axios('https://api.quotable.io/random').then((result) => {
      setAuthor(result.data.author);
      setQuotes(`"${result.data.content}"`);
    });

    const hours = parseFloat(currentTime);

    if (hours >= 5 && hours <= 11) {
      setDisplay(morning);
    } else if (hours >= 12 && hours <= 17) {
      setDisplay(afternoon);
    } else if (hours >= 18 && hours <= 21) {
      setDisplay(evening);
    } else if ((hours >= 22 && hours <= 23) || (hours >= 0 && hours <= 4)) {
      setDisplay(night);
    } else {
      setDisplay(loading);
    }
  }, [currentTime]);

  const content = {
    loading: {
      background: 'https://wallpaperaccess.com/full/1442131.jpg',
      greeting: 'Welcome',
      color: 'dark',
      quotes: { quotes, author },
      time: currentTime,
    },
    morning: {
      background: 'https://c4.wallpaperflare.com/wallpaper/741/22/369/digital-digital-art-artwork-fantasy-art-landscape-hd-wallpaper-preview.jpg',
      greeting: 'Good Morning',
      color: 'dark',
      quotes: { quotes, author },
      time: currentTime,
    },
    afternoon: {
      background: 'https://c4.wallpaperflare.com/wallpaper/375/414/323/time-laps-photo-of-sea-shore-during-golden-hour-wallpaper-preview.jpg',
      greeting: 'Good Afternoon',
      color: 'dark',
      quotes: { quotes, author },
      time: currentTime,
    },
    evening: {
      background: 'https://wallpaperaccess.com/full/546143.jpg',
      greeting: 'Good Evening',
      color: 'dark',
      quotes: { quotes, author },
      time: currentTime,
    },
    night: {
      background: 'https://wallpaperaccess.com/full/1631166.jpg',
      greeting: 'Good Night',
      color: 'light',
      quotes: { quotes, author },
      time: currentTime,
    },
  };

  const { loading, morning, afternoon, evening, night} = content;

  return (
    <div
      id='app'
      className='d-flex flex-column justify-content-between'
      style={{
        backgroundImage: `linear-gradient( rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3) ), url(${display.background})`,
      }}>
      <Quotes display={display} />
      <Time display={display} />
    </div>
  );
}

export default App;
